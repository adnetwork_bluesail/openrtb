package openrtb

// Publisher ...
type Publisher struct {
	ID   string   `json:"id" bson:"id"`
	Name string   `json:"name" bson:"name"`
	Cat  []string `json:"cat" bson:"cat"`
}
